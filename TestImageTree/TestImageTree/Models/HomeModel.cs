﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestImageTree.Models
{
    public class HomeModel
    {
        public HomeModel()
        {
            RowsCount = 3;
            ColumnsCount = 2;

            PaddingBottom = 10;
            PaddingLeft = 10;
            PaddingRight = 10;
            PaddingTop = 10;
        }

        [Required(ErrorMessage = "*")]
        public int Width { get; set; }

        [Required(ErrorMessage = "*")]
        public TestLevel Level { get; set; }

        public int RowsCount { get; set; }
        public int ColumnsCount { get; set; }

        public int PaddingTop { get; set; }
        public int PaddingRight { get; set; }
        public int PaddingBottom { get; set; }
        public int PaddingLeft { get; set; }

        public HttpFileCollectionBase Files { get; set; }
    }

    public enum TestLevel
    {
        [Display(Name = "Easy")]
        Easy = 1,
        [Display(Name = "Medium")]
        Medium = 2,
        [Display(Name = "Hard")]
        Hard = 3
    }
}