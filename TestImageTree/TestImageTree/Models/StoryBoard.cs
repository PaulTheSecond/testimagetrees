﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using TestImageTree.Helpers.TreeMap;

namespace TestImageTree.Models
{
    public class Frame
    {
        public Frame(int width)
        {
            Frames = new List<Frame>();
            Width = width;
        }
        public int Width { get; set; }
        public int Height { get; set; }
        public string StoryboardPath { get; set; }
        
        public IList<Frame> Frames { get; set; }
        public IList<SpecialImage> Images { get; set; }
        public SpecialImage ResultImage { get; set; }
    }
}