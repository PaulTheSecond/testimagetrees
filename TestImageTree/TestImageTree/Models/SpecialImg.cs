﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace TestImageTree.Models
{
    public class SpecialImage
    {
        public static int PaddingTop { get; set; }
        public static int PaddingRight { get; set; }
        public static int PaddingBottom { get; set; }
        public static int PaddingLeft { get; set; }

        public Image Value { get; set; }
        public bool IsBorderd { get; set; }
        public double Mutex { get; set; }

        public int X { get; set; }
        public int Y { get; set; }

        public int Height
        {
            get
            {
                return (int)(Value.Height * Mutex);
            }
        }
        public int Width
        {
            get
            {
                return (int)(Value.Width * Mutex);
            }
        }
    }
}