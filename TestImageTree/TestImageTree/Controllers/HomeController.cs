﻿using System.Web.Mvc;
using System.Web.Routing;
using TestImageTree.Helpers;
using TestImageTree.Models;

namespace TestImageTree.Controllers
{
    public class HomeController : Controller
    {
        private readonly HomeModel _model = new HomeModel();
        public ActionResult Index(TestLevel level = TestLevel.Medium, int width = 1500)
        {
            _model.Level = level;
            _model.Width = width;
            return View(_model);
        }

        [HttpPost]
        public ActionResult StoryBoard(HomeModel model)
        {
            if (Request.Files.Count == 0 || string.IsNullOrWhiteSpace(Request.Files[0].FileName) || (model.ColumnsCount + model.RowsCount > Request.Files.Count - 1 && model.Level != TestLevel.Easy))
            {
                return RedirectToAction("Index", "Home", new RouteValueDictionary() {{"level", model.Level}, {"width", model.Width}});
            }
            if (model.RowsCount == 1 || model.ColumnsCount == 0)
            {
                model.Level = TestLevel.Easy;
            }
            if (model.Level == TestLevel.Hard)
            {
                SpecialImage.PaddingBottom = model.PaddingBottom;
                SpecialImage.PaddingLeft = model.PaddingLeft;
                SpecialImage.PaddingRight = model.PaddingRight;
                SpecialImage.PaddingTop = model.PaddingTop;
            }
            else
            {
                SpecialImage.PaddingBottom = 0;
                SpecialImage.PaddingLeft = 0;
                SpecialImage.PaddingRight = 0;
                SpecialImage.PaddingTop = 0;
            }
            model.Files = Request.Files;
            var storyBoardCreator = AssociateCreator.Resolve(model.Level);
            var sb = storyBoardCreator.Create(model);
            return View(sb);
        }
    }
}