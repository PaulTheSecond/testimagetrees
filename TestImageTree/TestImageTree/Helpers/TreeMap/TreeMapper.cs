﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TestImageTree.Models;

namespace TestImageTree.Helpers.TreeMap
{
    public static class TreeMapper
    {
        public static IEnumerable<SpecialImage> GetSlicesManualy (IEnumerable<SpecialImage> slice, int width)
        {
            var prevHeight = 0;
            var prevWidth = 0;

            var cnt = 0;
            var nextPoint = new Point(0, 0);
            var tempImage = slice.Where(w => w.Height == slice.Max(m=>m.Height)).ToList();
            var tmp = slice.ToList();
            tempImage.ForEach(f => tmp.Remove(f));
            tempImage.ForEach(f=>tmp.Insert(cnt++,f));
            slice = tmp;
            cnt = 0;

            foreach (var img in slice)
            {
                cnt ++;                

                if (cnt == 1 || cnt > 2)
                {
                    if (cnt != 1 && cnt % 2 != 0)
                    {
                        nextPoint.Y = prevHeight - img.Height;
                    }
                    img.X = nextPoint.X;
                    img.Y = nextPoint.Y;
                    if (cnt > 2)
                    {
                        if (cnt % 2 == 0)
                        {
                            nextPoint.X += img.Width;
                            nextPoint.Y = prevHeight - img.Height;
                        }
                        else
                        {
                            nextPoint.Y = 0;
                        }
                    }
                }
                else
                {
                    img.X = width-img.Width;
                    img.Y = nextPoint.Y;

                    nextPoint.X += prevWidth;
                    nextPoint.Y = prevHeight - img.Height;
                }
                if (cnt == 1 || (cnt > 2 && cnt % 2 == 0))
                {
                    prevHeight = img.Height;
                    prevWidth = img.Width;
                }              

                yield return img;
            }
        }

        public static SpecialImage DrawStoryboard(IEnumerable<SpecialImage> images, int width, int height)
        {
            var font = new Font("Arial", 8);

            var bmp = new Bitmap(width, height);
            var gfx = Graphics.FromImage(bmp);

            gfx.FillRectangle(Brushes.Black, new RectangleF(0, 0, width, height));
            foreach (var i in images)
            {
                var img = ImageHelper.Resize(i);
                gfx.DrawImage(img.Value, new Rectangle(i.X, i.Y, i.Width, i.Height));
            }

            return new SpecialImage() { IsBorderd = true, Value = bmp, Mutex = 1 };
        }
    }
}
