﻿using TestImageTree.Models;

namespace TestImageTree.Helpers
{
    public interface IStoryBoardCreator
    {
        Frame Create(HomeModel data);
    }
}
