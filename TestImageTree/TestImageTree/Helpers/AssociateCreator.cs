﻿using System.Collections.Generic;
using TestImageTree.Models;

namespace TestImageTree.Helpers
{
    public class AssociateCreator
    {
        private static readonly Dictionary<TestLevel, IStoryBoardCreator> _associationTypes = new Dictionary<TestLevel, IStoryBoardCreator>
        {
            {TestLevel.Easy, new EasyStoryBoard()},
            {TestLevel.Medium, new MediumStoryBoard()},
            {TestLevel.Hard, new HardStoryBoard()}
        };

        public static IStoryBoardCreator Resolve(TestLevel level)
        {
            return _associationTypes[level];
        }
    }
}