﻿using System;
using System.Drawing;
using System.IO;
using System.Web;
using TestImageTree.Models;

namespace TestImageTree.Helpers
{
    public class EasyStoryBoard : IStoryBoardCreator
    {
        #region IStoryBoardCreator members

        public Frame Create(HomeModel data)
        {
            var sb = new Frame(data.Width);
            var newRow = new Frame(sb.Width);
            for (var i = 0; i < data.Files.Count; i++)
            {
                var httpPostedFileBase = data.Files[i];
                if (httpPostedFileBase != null && string.IsNullOrWhiteSpace(httpPostedFileBase.FileName))
                {
                    throw new ArgumentNullException("File not found or empty.");
                }
                newRow.Images.Add(new SpecialImage{Value = new Bitmap(httpPostedFileBase.InputStream)});
            }
            sb.Frames.Add(ImageHelper.PrepareRow(newRow));
            sb.ResultImage = sb.Frames[0].ResultImage;
            var src = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Storyboards/"),string.Format("sb.{0}.jpg", data.Level));
            var src2 = string.Format("~/Content/Storyboards/sb.{0}.jpg", data.Level);
            sb.ResultImage.Value.Save(src);
            sb.StoryboardPath = src2;
            return sb;
        }

        #endregion

        
    }
}