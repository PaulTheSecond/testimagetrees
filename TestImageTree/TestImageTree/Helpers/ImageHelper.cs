﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using TestImageTree.Models;

namespace TestImageTree.Helpers
{
    public static class ImageHelper
    {
        public static Frame PrepareRow(Frame row)
        {
            var result = new Frame(row.Width) { Images = new List<SpecialImage>(row.Images) };
            var minHeight = row.Images.Min(m => m.Value.Height);
            for (var i = 0; i < result.Images.Count; i++)
            {
                result.Images[i] = Resize(result.Images[i], 0, minHeight);
            }
            result.ResultImage = Resize(GetRow(result.Images.ToList()), row.Width, 0);
            return result;
        }

        public static SpecialImage GetRow(List<SpecialImage> src)
        {
            var newRowImage = new SpecialImage()
            {
                Value = new Bitmap(src.Aggregate(0, (w, img) => w + img.Value.Width), src.Max(m=>m.Value.Height)),
                IsBorderd=true
            };
            var graph = Graphics.FromImage(newRowImage.Value);
            int width = 0;
            foreach (SpecialImage img in src)
            {
                graph.DrawImage(img.Value,width,0);
                width += img.Value.Width;
            }
            graph.Dispose();
            return newRowImage;
        }

        public static SpecialImage Resize(SpecialImage sourceImage, int newWidth, int newHeight)
        {
            var newLogo = new SpecialImage()
            {
                Value = new Bitmap(
                    (newWidth <= 0 ? sourceImage.Value.Width*newHeight/sourceImage.Value.Height : newWidth) - SpecialImage.PaddingLeft-SpecialImage.PaddingRight,
                    (newHeight <= 0 ? sourceImage.Value.Height*newWidth/sourceImage.Value.Width : newHeight) - SpecialImage.PaddingBottom - SpecialImage.PaddingTop
                    ),
                    IsBorderd=sourceImage.IsBorderd
            };
            var graph = Graphics.FromImage(newLogo.Value);
            graph.InterpolationMode = InterpolationMode.Bicubic;

            graph.DrawImage(sourceImage.Value,
                newWidth!=0
                    ? new Rectangle(0, 0, newWidth, sourceImage.Value.Height * newWidth / sourceImage.Value.Width)
                    : new Rectangle(0, 0, sourceImage.Value.Width * newHeight / sourceImage.Value.Height, newHeight));

            graph.DrawImageUnscaledAndClipped(newLogo.Value, new Rectangle(0, 0, newWidth, newHeight));

            return CreateBorders(newLogo);
        }

        public static SpecialImage Resize(SpecialImage sourceImage)
        {
            int newWidth = (int)Math.Round(sourceImage.Mutex * sourceImage.Value.Width) - SpecialImage.PaddingLeft - SpecialImage.PaddingRight;
            int newHeight = (int)Math.Round(sourceImage.Mutex * sourceImage.Value.Height) - SpecialImage.PaddingBottom - SpecialImage.PaddingTop;
            var newLogo = new SpecialImage()
            {
                Value = new Bitmap(newWidth,newHeight)
            };
            var graph = Graphics.FromImage(newLogo.Value);
            graph.InterpolationMode = InterpolationMode.Bicubic;

            graph.DrawImage(sourceImage.Value, new Rectangle(0, 0, newWidth, newHeight));

            graph.DrawImageUnscaledAndClipped(newLogo.Value, new Rectangle(0, 0, newWidth, newHeight));

            return CreateBorders(newLogo);
        }

        private static SpecialImage CreateBorders(SpecialImage src)
        {
            if (src.IsBorderd)
            {
                return src;
            }
            var newColImage = new Bitmap(src.Value.Width + SpecialImage.PaddingLeft + SpecialImage.PaddingRight, src.Value.Height + SpecialImage.PaddingBottom + SpecialImage.PaddingTop);
            var graph = Graphics.FromImage(newColImage);
            if (SpecialImage.PaddingTop > 0)
            {
                graph.FillRectangle(new SolidBrush(Color.Black), 0, 0, newColImage.Width, newColImage.Height);
            }
            graph.DrawImage(src.Value, SpecialImage.PaddingTop, SpecialImage.PaddingLeft);
            graph.Dispose();

            return new SpecialImage{IsBorderd=true, Value = newColImage};
        }
    }
}