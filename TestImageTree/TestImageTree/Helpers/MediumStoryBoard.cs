﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using TestImageTree.Helpers.TreeMap;
using TestImageTree.Models;

namespace TestImageTree.Helpers
{
    public class MediumStoryBoard:IStoryBoardCreator
    {
        public SpecialImage[] Images;

        #region IStoryBoardCreator members

        public virtual Frame Create(HomeModel data)
        {
            CleanUp();
            var sb = new Frame(data.Width);

            Images = new SpecialImage[7];

            for (var i = 0; i < 7; i++)
            {
                var httpPostedFileBase = data.Files[i];
                if (httpPostedFileBase != null && string.IsNullOrWhiteSpace(httpPostedFileBase.FileName))
                {
                    throw new ArgumentNullException("File not found or empty.");
                }
                Images[i] = new SpecialImage() { IsBorderd = false, Value = new Bitmap(httpPostedFileBase.InputStream) };
            }
            Images = Images.OrderByDescending(od => od.Value.Height * od.Mutex).ToArray();

            var matrix = new double[,]
            {   //  a                           b                               c                            d                           e                           f                            g
                {Images[0].Value.Width,     Images[1].Value.Width,      Images[2].Value.Width,      0.0,                        0.0,                        0.0,                        0.0 },
                {0.0,                       Images[1].Value.Width,      0.0,                        -Images[3].Value.Width,     -Images[4].Value.Width,     0.0,                        0.0 },
                {0.0,                       0.0,                        0.0,                        0.0,                        Images[4].Value.Width,      -Images[5].Value.Width,     -Images[6].Value.Width },
                {Images[0].Value.Height,    -Images[1].Value.Height,    0.0,                        -Images[3].Value.Height,    0.0,                        0.0,                        0.0 },
                {0.0,                       -Images[1].Value.Height,    Images[2].Value.Height,     0.0,                        -Images[4].Value.Height,    0.0,                        -Images[6].Value.Height },
                {0.0,                       0.0,                        0.0,                        Images[3].Value.Height,     -Images[4].Value.Height,    -Images[5].Value.Height,    0.0 },
                {0.0,                       0.0,                        0.0,                        0.0,                        0.0,                        Images[5].Value.Height,    -Images[6].Value.Height }  
            };

            var b_vector = new double[] 
            { 
                data.Width,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0
            };

            var GaussResult = new Gauss.LinearSystem(matrix, b_vector);

            for (var idx = 0; idx < Images.Length; idx++)
            {
                Images[idx].Mutex = GaussResult.XVector[idx];
            }

            sb.Height = (int)(Images[0].Value.Height * Images[0].Mutex);
            
            sb.ResultImage = TreeMapper.DrawStoryboard(TreeMapper.GetSlicesManualy(Images, sb.Width), sb.Width, sb.Height);

            var src = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Storyboards/"), string.Format("sb.{0}.jpg", data.Level));
            var src2 = string.Format("~/Content/Storyboards/sb.{0}.jpg", data.Level);
            sb.ResultImage.Value.Save(src);
            sb.StoryboardPath = src2;

            return sb;
        }

        private void CleanUp()
        {
            if (Images != null)
            {
                Images = null;
            }
        }

        #endregion
    }
}